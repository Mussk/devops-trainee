#!/bin/bash

#JAVA INSTALL
sudo apt update 
sudo apt install -y default-jre
sudo apt install -y default-jdk
sudo apt install -y maven

sudo apt install -y unzip

sudo apt install -y npm

sudo mv /home/vagrant/id_rsa /home/devops/.ssh/id_rsa
sudo chmod 600 /home/devops/.ssh/id_rsa
sudo chown devops /home/devops/.ssh/id_rsa

sudo mv /home/vagrant/id_rsa.pub /home/devops/.ssh/id_rsa.pub
sudo chown devops /home/devops/.ssh/id_rsa.pub

#JENKINS INSTALL
wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key | sudo apt-key add -
sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt update 
sudo apt install -y jenkins
sudo systemctl start jenkins
sudo systemctl status jenkins
sudo ufw allow 8080
sleep 20

#DOCKER INSTALL
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository \
"deb [arch=amd64] https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) \
stable"
sudo apt-get install -y docker-ce docker-ce-cli containerd.io

sudo mv /home/vagrant/jenkins_plugin.sh /home/devops/jenkins_plugin.sh
sudo bash /home/devops/jenkins_plugin.sh docker-commons docker-workflow ssh-steps
echo 'THIS IS PASSWORD FOR JENKINS SERVER:'
sudo cat /var/lib/jenkins/secrets/initialAdminPassword

sudo usermod -a -G sudo jenkins
sudo chmod 666 /var/run/docker.sock
sudo docker login -u mussk -p 68rh49FlxmMq
