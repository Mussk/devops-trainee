#!/bin/bash

cat /home/vagrant/.ssh/id_rsa.pub >> /home/vagrant/.ssh/authorized_keys
useradd -m -s /bin/bash -U devops -u 666
sudo usermod -a -G sudo devops
cp -pr /home/vagrant/.ssh /home/devops/
chown -R devops:devops /home/devops
echo "%devops ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/devops

#EXPECT INSTALLATION
sudo apt-get update -y
sudo apt-get install -y expect

