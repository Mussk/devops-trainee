#
# This is an example VCL file for Varnish.
#
# It does not do anything by default, delegating control to the
# builtin VCL. The builtin VCL is called when there is no explicit
# return statement.
#
# See the VCL chapters in the Users Guide at https://www.varnish-cache.org/docs/
# and https://www.varnish-cache.org/trac/wiki/VCLExamples for more examples.

# Marker to tell the VCL compiler that this VCL has been adapted to the
# new 4.0 format.
vcl 4.0;

# Default backend definition. Set this to point to your content server.
backend nginx {
	.host = "192.168.1.201";
	.port = "80";
}
backend apache {
	.host = "192.168.1.201";
	.port = "81";
}
backend tomcat {
    .host = "192.168.1.201";
    .port = "82";
}
backend superv {
    .host = "192.168.1.201";
    .port = "83";
}
backend node {
    .host = "192.168.1.201";
    .port = "84";
}
sub vcl_recv {
	unset req.http.x-cache;
	if(req.http.host == "nginx.devops-trainee.edu") {
		set req.backend_hint = nginx;
	}
	if(req.http.host == "apache.devops-trainee.edu") {
		set req.backend_hint = apache;
	}
	if(req.http.host == "tomcat.devops-trainee.edu") {
                set req.backend_hint = tomcat;
        }
	if(req.http.host == "superv.devops-trainee.edu") {
                set req.backend_hint = superv;
        }
	if(req.http.host == "node.devops-trainee.edu") {
                set req.backend_hint = node;
        }
    # Happens before we check if we have this in cache already.
    #
    # Typically you clean up the request here, removing cookies you don't need,
    # rewriting the request, etc.
}

sub vcl_hit {
	set req.http.x-cache = "hit";
}

sub vcl_miss {
	set req.http.x-cache = "miss";
}

sub vcl_pass {
	set req.http.x-cache = "pass";
}

sub vcl_pipe {
	set req.http.x-cache = "pipe uncacheable";
}

sub vcl_synth {
	set req.http.x-cache = "synth synth";
	# uncomment the following line to show the information in the response
	set resp.http.x-cache = req.http.x-cache;
}
	
sub vcl_backend_response {
    # Happens after we have read the response headers from the backend.
    #
    # Here you clean the response headers, removing silly Set-Cookie headers
    # and other mistakes your backend does.
	if(bereq.backend == nginx) {
		unset beresp.http.set-cookie;
		set beresp.ttl = 10s;
}
	if(bereq.backend == apache) {
		unset beresp.http.set-cookie;
		set beresp.ttl = 0s;
}
	if(bereq.backend == tomcat) {
		unset beresp.http.set-cookie;
		set beresp.ttl = 20s;
}
	if(bereq.backend == superv) {
		unset beresp.http.set-cookie;
		set beresp.ttl = 60s;
}
	if(bereq.backend == node) {
		unset beresp.http.set-cookie;
		set beresp.ttl = 30s;
}

}

sub vcl_deliver {
    # Happens when we have all the pieces we need, and are about to send the
    # response to the client.
    #
    # You can do accounting or modifying the final object here.
	if (obj.uncacheable) {
		set req.http.x-cache = req.http.x-cache + " uncacheable" ;
	} else {
		set req.http.x-cache = req.http.x-cache + " cached" ;
	}
	# uncomment the following line to show the information in the response
	 set resp.http.x-cache = req.http.x-cache;
}
