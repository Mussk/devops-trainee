#!/bin/bash

#=====================================

#CONFSCRIPT FOR VARNISH 

#=====================================

#VARIABLES

sudo sed -i 's|DAEMON_OPTS="-a :6081 |DAEMON_OPTS="-a :80 |g' /etc/defult/varnish

sudo cp /lib/systemd/system/varnish.service /etc/systemd/system/

sudo sed -i 's|ExecStart=/usr/sbin/varnishd -j unix,user=vcache -F -a :6081 -T localhost:6082 -f /etc/varnish/default.vcl -S /etc/varnish/secret -s malloc,256m|ExecStart=/usr/sbin/varnishd -j unix,user=vcache -F -a :80 -T localhost:6082 -f /etc/varnish/default.vcl -S /etc/varnish/secret -s malloc,256m|g' \
/etc/systemd/system/varnish.service

mv /home/vagrant/default.vcl /etc/varnish/default.vcl

sudo systemctl daemon-reload

sudo systemctl restart varnish

#logs to file
sudo touch /etc/varnish/varnish.log
sudo touch /etc/varnish/varnishncsa.log

sudo varnishlog -A -D -b -w /etc/varnish/varnish.log
sudo varnishncsa -a -D -b -w /etc/varnish/varnishncsa.log
