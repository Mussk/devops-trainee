#!/bin/bash

#=====================================

#CONFSCRIPT FOR HAPROXY

#=====================================

#VARIABLES
DOMAIN=$1

printf "ENABLED=1" | sudo tee -a /etc/default/haproxy
sudo mv /home/vagrant/$DOMAIN.pem /etc/ssl/certs/$DOMAIN.pem
sudo mv -f /home/vagrant/haproxy.cfg /etc/haproxy/haproxy.cfg

sudo systemctl restart haproxy