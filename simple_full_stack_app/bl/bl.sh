#!/bin/bash

#=====================================

#INSTALLATION SCRIPT FOR BL 

#=====================================

#VARIABLES

#VARNISH INSTALLATION
curl -L https://packagecloud.io/varnishcache/varnish41/gpgkey | sudo apt-key add -
sudo nano /etc/apt/sources.list.d/varnishcache_varnish41.list
deb https://packagecloud.io/varnishcache/varnish41/ubuntu/ trusty main
deb-src https://packagecloud.io/varnishcache/varnish41/ubuntu/ trusty main

sudo apt-get update
sudo apt-get install varnish -y

#HAPROXY INSTALLATION
sudo apt-get install haproxy -y