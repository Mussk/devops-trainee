#!/bin/bash

#VARIABLES
pass=$1
db_val=$2
db_name=$3
field_name=$4
tb_name_one=$5
tb_name_two=$6
user_one=$7
user_two=$8
ip_addr=$9


#CONFING my.cnf file
printf "\n[mysqld]\n\
skip-networking=0\n\
skip-bind-address\n" | sudo tee -a /etc/mysql/my.cnf

#ENABLE LOGS
printf "\
general_log=1\n\
general_log_file=/var/log/mysql/mysql.log\n\
slow_query_log=1\n\
slow_query_log_file=/var/log/mysql/mysql-slow.log\n\
log_queries_not_using_indexes=1\n\
"| sudo tee -a /etc/mysql/my.cnf

#MYSQL DB CREATION PART
mysql -u root -p${pass} <<CONF
CREATE DATABASE ${db_name};
USE ${db_name};
CREATE TABLE ${tb_name_one} (${field_name} VARCHAR(30));
CREATE TABLE ${tb_name_two} (${field_name} VARCHAR(30));
INSERT INTO ${tb_name_one} (${field_name}) VALUES ('${db_val}');
INSERT INTO ${tb_name_two} (${field_name}) VALUES ('${db_val}');
COMMIT;
CONF

#USERS PART
mysql -u root -p${pass} <<CONF
use mysql;
CREATE USER '${user_one}'@'${ip_addr}' IDENTIFIED BY '${pass}';
CREATE USER '${user_two}'@'${ip_addr}' IDENTIFIED BY '${pass}';
GRANT ALL ON ${db_name}.${tb_name_one} TO '${user_one}'@'${ip_addr}';
GRANT ALL ON ${db_name}.${tb_name_two} TO '${user_two}'@'${ip_addr}';
FLUSH PRIVILEGES;
SELECT user,host,password FROM mysql.user;
CONF

sudo systemctl restart mysql