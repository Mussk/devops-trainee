#!/bin/bash

#VARIABLES
pass=$1

#================================================

#MYQSL INSTALLATION AND CONFIGURATION

#================================================

#REPO 
sudo apt update
MARIADB=$(expect -c "
set timeout -1
spawn sudo apt install mariadb-server mariadb-client
expect \"Do you want to continue?\"
send \"Y\r\"
expect eof
")
echo "$MARIADB"
sudo apt-get install software-properties-common
sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
sudo add-apt-repository 'deb [arch=amd64,i386] http://mirror.klaus-uwe.me/mariadb/repo/5.5/ubuntu yakkety main'
sudo apt update

# SETTING UP A NON-INTERACTIVE INSTALLATION
export DEBIAN_FRONTEND="noninteractive"
sudo debconf-set-selections <<< 'mariadb-server-10.5.4 mysql-server/root_password password '"$pass"
sudo debconf-set-selections <<< 'mariadb-server-10.5.4 mysql-server/root_password_again password '"$pass"
echo "$MARIADB"

#mysql_secure_installation script
SECURE_MYSQL=$(expect -c "
set timeout 10
spawn mysql_secure_installation
expect \"Enter current password for root (enter for none):\"
send \"$pass\r\"
expect \"Change the root password?\"
send \"n\r\"
expect \"Remove anonymous users?\"
send \"y\r\"
expect \"Disallow root login remotely?\"
send \"y\r\"
expect \"Remove test database and access to it?\"
send \"y\r\"
expect \"Reload privilege tables now?\"
send \"y\r\"
expect eof
")

echo "$SECURE_MYSQL"



#================================================

#MONGODB INSTALLATION AND CONFIGURATION

#================================================

#INSTALLATION

wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list
sudo apt-get update
sudo apt-get install -y mongodb-org
sudo systemctl start mongod
sleep 3








