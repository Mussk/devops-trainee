#!/bin/bash

#VARIABLES
pass=$1
db_val=$2
db_name=$3
field_name=$4
tb_name_one=$5
tb_name_two=$6
user_one=$7
user_two=$8
db_ip=$9

#CONFIURATION
mongo <<EOF
use admin
db.createUser(\
    {\
        user: "admin",\
        pwd: "${pass}",\
        roles: [ { role: "userAdminAnyDatabase", db: "admin" },\
                 { role: "readWrite", db: "${db_name}" } ]\
}\
)
exit
EOF

sudo sed -i "s/bindIp: 127.0.0.1/\
bindIp: 127.0.0.1,${db_ip}/g" /etc/mongod.conf
echo -e "security:\n  authorization: \"enabled\"" >> /etc/mongod.conf
sudo systemctl restart mongod
sleep 3
mongo -u admin --password=${pass} --authenticationDatabase admin <<EOF
use ${db_name}
db.createCollection("${tb_name_one}")
db.createCollection("${tb_name_two}")
db.${tb_name_one}.insert({"${field_name}" : "${db_val}"})
db.${tb_name_two}.insert({"${field_name}" : "${db_val}"})

db.createUser(\
    {\
        user: "${user_one}",\
        pwd: "${pass}",\
        roles: [\
            { role: "readWrite", db: "${db_name}" }\
        ]\
}\
)

db.createUser(\
    {\
        user: "${user_two}",\
        pwd: "${pass}",\
        roles: [\
            {role: "readWrite", db: "${db_name}" }\
        ]\
}\
)
exit
EOF

sudo systemctl restart mongod
sudo systemctl enable mongod