import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

public class MyServlet extends javax.servlet.http.HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) {

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        try (PrintWriter writer = response.getWriter()) {
            DB db = new DB();
            writer.println("<!DOCTYPE html><html>");
            writer.println("<head>");
            writer.println("<meta charset=\"UTF-8\" />");
            writer.println("<title>Tomcat: Hello world!</title>");
            writer.println("</head>");
            writer.println("<body>");
            writer.println("<h1>" + db.getResultQuery("SELECT data FROM tomcat_java") + "</h1>");
            writer.println("<h2>Hello, Exadel!</h2>");
            writer.println("</body>");
            writer.println("</html>");

            db.getConn().close();

        }catch (Exception ex){}
    }

}
