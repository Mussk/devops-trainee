from flask import Flask
from flask_pymongo import PyMongo
from bson.json_util import dumps
import json
import ast

app = Flask(__name__)
app.config['MONGO_URI'] = 'mongodb://superv-user:11111111@192.168.1.202:27017/hellodb'
mongo = PyMongo(app)
@app.route('/')
def home_page():
        query = mongo.db.superv_python.find_one_or_404({"data": "Hello, world"})
        dict = ast.literal_eval(dumps(query))
        res = dict.get("data")
        return res

if __name__ == '__main__':
        app.run(debug=False,host='0.0.0.0')