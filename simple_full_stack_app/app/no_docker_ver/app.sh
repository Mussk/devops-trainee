#!/bin/bash

#=====================================

#INSTALLATION SCRIPT FOR APP SERVICES

#=====================================

#VARIABLES
tomcat_link="https://apache.ip-connect.vn.ua/tomcat/tomcat-9/v9.0.37/bin/apache-tomcat-9.0.37.tar.gz"
tomcat_file_name="apache-tomcat-9.0.37.tar.gz"

#NGINX INSTALLATION
sudo apt update

sudo apt install -y nginx

sudo ufw allow 'Nginx HTTP'
sudo systemctl start nginx
sudo systemctl enable nginx
sudo systemctl status nginx

#APACHE INSTALLATION
sudo apt update

sudo apt install -y apache2 


sudo ufw allow 'Apache'
sudo systemctl enable apache2

#TOMCAT INSTALLATION
cd /tmp
curl -0 $tomcat_link -o $tomcat_file_name
sudo mkdir /opt/tomcat
sudo tar xzvf $tomcat_file_name -C /opt/tomcat --strip-components=1
cd /home/vagrant

#JAVA INSTALL
sudo apt update

echo "Starting download java JDK (may take some time)..."

sudo apt install -y default-jdk


#SUPERVISORD INSTALL

sudo apt-get install -y supervisor


#VENV INSTALL

sudo apt install -y python3-venv

#NODE.JS INSTALL

curl -sL https://deb.nodesource.com/setup_current.x | sudo -E bash -
sudo apt-get install -y nodejs
apt-get install -y build-essential
