#!/bin/bash

#=====================================

#FLASK CONFIGURATION

#=====================================

#VARIABLES
username=$1
#sudo mkdir -p /srv/Supervisord/python-app
#cd /srv/Supervisord/python-app
sudo -u $username mkdir -p /home/$username/python
cd /home/$username/python
sudo -u $username python3 -m venv venv
sudo -u $username mkdir -p /home/devops/python/venv/Scripts/python-app
sudo cp -p /home/vagrant/python-app.py /home/devops/python/venv/Scripts/python-app/python-app.py
source venv/bin/activate
pip install Flask
pip install Flask-PyMongo
export FLASK_APP=/home/devops/python/venv/Scripts/python-app/python-app.py
deactivate
#allow ports < 1024
sudo setcap cap_net_bind_service=+ep $(readlink -f /usr/bin/python3)

sudo systemctl restart supervisor

sudo mkdir -p /srv/Supervisord/python-app
sudo mv /home/vagrant/python-app.py /srv/Supervisord/python-app/python-app.py

