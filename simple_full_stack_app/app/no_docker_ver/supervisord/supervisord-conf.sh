#!/bin/bash

#=====================================

#SUPERVISORD CONFIGURATION

#=====================================

#VARIABLES
ip_address_adm_pan=$1
port_adm_pan=$2
username=$3
pass=$4
app_name=$5
ip_address_web_server=$6
port_web_server=$7

#SUPERVISORD CONFIGURATION
sudo rm /etc/supervisor/supervisord.conf
echo_supervisord_conf | sudo tee -a  /etc/supervisor/supervisord.conf

printf "\n[inet_http_server]\n\
port=${ip_address_adm_pan}:${port_adm_pan}\n\
username=${username}\n\
password=${pass}\n\n\
[include]\n\
files=conf.d/*.conf\n" | sudo tee -a /etc/supervisor/supervisord.conf

sudo supervisorctl reread
sudo supervisorctl update

#CREATE LOGS DIR
sudo mkdir -p /etc/supervisor/logs

#python-app.conf CONFIGURATION

sudo touch /etc/supervisor/conf.d/$app_name.conf

printf "; LOCATE FOR THE VirtualEnv PROGRAM TO RUN :\n\
[program:${app_name}]\n\
user=${username}\n\
command=/home/${username}/python/venv/bin/flask run --host=${ip_address_web_server} --port=${port_web_server}     
autostart=true\n\
autorestart=true\n\
startretries=3\n\
stopasgroup=true\n\
environment=FLASK_APP=/home/${username}/python/venv/Scripts/python-app/python-app\n\
; DISABLE THIS IT WILL GROW BIGGER, FOR TESTING JUST ENABLE THIS\n\
stderr_logfile=/etc/supervisor/logs/python-app_supervisor.err.log\n\
stdout_logfile=/etc/supervisor/logs/python-app_supervisor.out.log\n" | sudo tee -a /etc/supervisor/conf.d/python-app.conf




