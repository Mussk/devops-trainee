#!/bin/bash

#=====================================

#TOMCAT CONFIGURATION

#=====================================

#VARIABLES
#$1 = username
#$2 = pass
username=$1
pass=$2


sudo groupadd tomcat
sudo useradd -s /bin/false -g tomcat -d /opt/tomcat tomcat

cd /opt/tomcat
sudo chgrp -R tomcat /opt/tomcat
sudo chmod -R g+r conf
sudo chmod g+x conf
sudo chown -R tomcat webapps/ work/ temp/ logs/

sudo mv /home/vagrant/tomcat.service /etc/systemd/system/tomcat.service
sudo sed -i 's|<Connector port="8080" protocol="HTTP/1.1"|<Connector port="82" protocol="HTTP/1.1"|g' /opt/tomcat/conf/server.xml
sudo systemctl daemon-reload
sudo systemctl enable tomcat

sudo ufw allow 82

sudo sed -i '/version="1.0">/a <user username="'"$username"'" password="'"$pass"'" roles="manager-gui,admin-gui"/>' /opt/tomcat/conf/tomcat-users.xml

sudo sed -i 's/<Valve className="org.apache.catalina.valves.RemoteAddrValve"/\
<!--<Valve className="org.apache.catalina.valves.RemoteAddrValve"/g' /opt/tomcat/webapps/manager/META-INF/context.xml
sudo sed -i '/allow="127\.\d+\.\d+\.\d+\|::1\|0:0:0:0:0:0:0:1" \/>/ s/$/-->/' /opt/tomcat/webapps/manager/META-INF/context.xml
sudo sed -i 's/<Valve className="org.apache.catalina.valves.RemoteAddrValve"/\
<!--<Valve className="org.apache.catalina.valves.RemoteAddrValve"/g' /opt/tomcat/webapps/host-manager/META-INF/context.xml
sudo sed -i '/allow="127\.\d+\.\d+\.\d+\|::1\|0:0:0:0:0:0:0:1" \/>/ s/$/-->/' /opt/tomcat/webapps/host-manager/META-INF/context.xml

sudo setcap cap_net_bind_service+ep /usr/lib/jvm/java-1.11.0-openjdk-amd64/bin/java

sudo systemctl stop tomcat

#DEPLOY JAVA-APP
sudo rm -fr /opt/tomcat/webapps/*
sudo mv /home/vagrant/java-app_war.war /home/vagrant/ROOT.war 
sudo mv /home/vagrant/ROOT.war /opt/tomcat/webapps/ROOT.war

sudo systemctl start tomcat

sudo mkdir -p /srv/Tomcat
sudo mv /home/vagrant/java-app /srv/Tomcat/java-app

sudo systemctl restart tomcat