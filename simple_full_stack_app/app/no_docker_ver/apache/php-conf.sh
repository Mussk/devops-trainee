#!/bin/bash

#=====================================

#PHP CONFIGURATION

#=====================================

#VARIABLES
#$1 = server domain
#$2 = script name (with extension)


sudo apt-get install -y php libapache2-mod-php php-mysql


sudo sed -i 's/DirectoryIndex index.html index.cgi index.pl index.php index.xhtml index.htm/\
DirectoryIndex index.php index.html index.cgi index.pl index.xhtml index.htm/g' /etc/apache2/mods-enabled/dir.conf

mv /home/vagrant/"$2" /var/www/"$1"/html/"$2"

sudo mkdir -p /srv/Apache/php-app
sudo cp -p /var/www/"$1"/html/"$2" /srv/Apache/php-app/"$2"

sudo systemctl restart apache2
sleep 3
sudo systemctl status apache2