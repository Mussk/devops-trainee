#!/bin/bash

#=====================================

#APACHE CONFIGURATION

#=====================================

#VARIABLES
#$1 = server domain

#VIRTUAL HOSTS

sudo mkdir -p /var/www/"$1"/html
sudo chown -R $USER:$USER /var/www/"$1"/html
sudo chmod -R 755 /var/www/"$1"

mv /home/vagrant/"$1".conf /etc/apache2/sites-available/"$1".conf

sudo a2ensite "$1".conf
sudo a2dissite 000-default.conf
sudo sed -i 's/Listen 80/Listen 81/g' /etc/apache2/ports.conf
sudo apache2ctl configtest
sudo systemctl restart apache2
