#!/bin/bash

#=====================================

#NODE.JS CONFIGURATION

#=====================================

#VARIABLES
username=$1

sudo mkdir -p /home/devops/express-app
cd /home/devops/express-app
npm init -y
npm install mongodb --save
sudo mv /home/vagrant/index.js /home/${username}/express-app/index.js

sudo npm install forever -g

sudo setcap cap_net_bind_service+ep /usr/bin/node

sudo mkdir -p /srv/Node.js/express-app
sudo cp -r /home/${username}/express-app /srv/Node.js/express-app

#execute and enable logging 
sudo forever start --append --fifo --logFile /home/${username}/express-app/forever.log --outFile /home/${username}/express-app/out.log --errFile /home/${username}/express-app/err.log /home/${username}/express-app/index.js


#write out current crontab
sudo crontab -u root -l > mycron
#echo new cron into cron file
echo "@reboot sudo /usr/bin/forever start --append --fifo --logFile /home/${username}/express-app/forever.log --outFile /home/${username}/express-app/out.log --errFile /home/${username}/express-app/err.log /home/${username}/express-app/index.js
" >> mycron
#install new cron file
sudo crontab -u root mycron
rm mycron
