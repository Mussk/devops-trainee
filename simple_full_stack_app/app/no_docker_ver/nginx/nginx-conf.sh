#!/bin/bash

#=====================================

#NGINX CONFIGURATION

#=====================================

#VARIABLES
#$1 = server domain

#SERVER BLOCKS

sudo mkdir -p /var/www/"$1"/html
sudo chown -R $USER:$USER /var/www/"$1"/html
sudo chmod -R 755 /var/www/"$1"

mv /home/vagrant/index.html /var/www/"$1"/html/index.html
mv /home/vagrant/"$1" /etc/nginx/sites-available/"$1"
sudo cp -p /var/www/"$1"/html/index.html /var/www/html/index.html 

sudo ln -s /etc/nginx/sites-available/"$1" /etc/nginx/sites-enabled/
sed -i 's/# server_names_hash_bucket_size 64;/server_names_hash_bucket_size 64;/g' /etc/nginx/nginx.conf

sudo nginx -t

sudo systemctl restart nginx

sudo mkdir -p /srv/Nginx/index
sudo cp -p /var/www/"$1"/html/index.html /srv/Nginx/index/index.html
